# Python program to demonstrate
# selenium
import time
import unittest
from selenium import webdriver
import string
import random

def login(driver):
    driver.find_element_by_name("username").clear()
    driver.find_element_by_name("username").send_keys("admin")
    driver.find_element_by_name("password").clear()
    driver.find_element_by_name("password").send_keys("admin")
    driver.find_element_by_id("btn-login").click()
    time.sleep(1)

def create_input(length):
    return ''.join(random.choice(string.ascii_letters) for i in range(length))

class ChallengeIntegrationUnitTesting(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.url = "http://localhost:5500/frontend/www/login.html"
        self.driver.get(self.url)
        self.driver.maximize_window()
        login(self.driver)
        self.url = "http://localhost:5500/frontend/www/myathletes.html"
        self.driver.get(self.url)
        self.challenge_title = create_input(4)
        time.sleep(2)

    def test_integration_challenge(self):
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(2)
        self.driver.find_element_by_id("challenge-title-input").clear()
        self.driver.find_element_by_id("challenge-title-input").send_keys(self.challenge_title)

        self.driver.find_element_by_id("challenge-user-input").clear()
        self.driver.find_element_by_id("challenge-user-input").send_keys("niko, ian")

        self.driver.find_element_by_id("button-submit-challenge").click()
        time.sleep(2)
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(2)

        list = self.driver.find_elements_by_class_name("card-body")
        for el in list:
            if self.challenge_title in el.text:
                self.assertTrue(True)
                break
        
    # cleanup method called after every test performed
    def tearDown(self):
        self.driver.close()
    
if __name__ == "__main__":
    unittest.main()
