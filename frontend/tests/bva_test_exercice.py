# Python program to demonstrate
# selenium
import time
import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import string
import random


working_inputs = [{
    "input": "name",
    "value": "name",
},
{
    "input": "description",
    "value": "description",
},
{
    "input": "unit",
    "value": "reps",
},
{
    "input": "duration",
    "value": "10",
},
{
    "input": "calories",
    "value": "200",
},
{
    "input": "muscleGroup",
    "value": "Legs",
},
]

def create_input(length, is_number=False):
    return ''.join(random.choice(string.digits if is_number else string.ascii_letters) for i in range(length))

def insert_valid_inputs(driver):
    for input in working_inputs:
        if input["input"] == "muscleGroup":
            select = Select(driver.find_element_by_name('muscleGroup'))
            select.select_by_visible_text('Legs')            
            continue
        
        driver.find_element_by_name(input["input"]).clear()
        driver.find_element_by_name(input["input"]).send_keys(input["value"])

def login(driver):
    driver.find_element_by_name("username").clear()
    driver.find_element_by_name("username").send_keys("admin")
    driver.find_element_by_name("password").clear()
    driver.find_element_by_name("password").send_keys("admin")
    driver.find_element_by_id("btn-login").click()
    time.sleep(1)




class ExerciseBVAUnitTesting(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.url = "http://localhost:5500/frontend/www/login.html"
        self.driver.get(self.url)
        self.driver.maximize_window()
        login(self.driver)
        self.url = "http://localhost:5500/frontend/www/exercise.html"
        self.driver.get(self.url)

    def test_unit_max(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("unit").clear()
        self.driver.find_element_by_name("unit").send_keys(create_input(51, is_number=True))
        self.driver.find_element_by_id("btn-ok-exercise").click()
        time.sleep(3)
        assert self.driver.current_url == self.url

    def test_unit_min(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("unit").clear()
        self.driver.find_element_by_name("unit").send_keys(create_input(49, is_number=True))
        self.driver.find_element_by_id("btn-ok-exercise").click()
        time.sleep(3)
        assert self.driver.current_url != self.url

    def test_duration_pos(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("duration").clear()
        self.driver.find_element_by_name("duration").send_keys(1)
        self.driver.find_element_by_id("btn-ok-exercise").click()
        time.sleep(3)
        assert self.driver.current_url != self.url

    def test_duration_neg(self):
        # It is possible to create an exercise with negative duration
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("duration").clear()
        self.driver.find_element_by_name("duration").send_keys(-1)
        self.driver.find_element_by_id("btn-ok-exercise").click()
        time.sleep(3)
        assert self.driver.current_url == self.url

    def test_duration_max(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("duration").clear()
        self.driver.find_element_by_name("duration").send_keys(create_input(9, is_number=True))
        self.driver.find_element_by_id("btn-ok-exercise").click()
        time.sleep(3)
        assert self.driver.current_url != self.url
    def test_duration_min(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("duration").clear()
        self.driver.find_element_by_name("duration").send_keys(create_input(0, is_number=True))
        self.driver.find_element_by_id("btn-ok-exercise").click()
        time.sleep(3)
        assert self.driver.current_url == self.url

    

    def test_calories_pos(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("calories").clear()
        self.driver.find_element_by_name("calories").send_keys(1)
        self.driver.find_element_by_id("btn-ok-exercise").click()
        time.sleep(3)
        assert self.driver.current_url != self.url

    def test_calories_neg(self):
        # It is possible to create an exercise with negative calories
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("calories").clear()
        self.driver.find_element_by_name("calories").send_keys(-1)
        self.driver.find_element_by_id("btn-ok-exercise").click()
        time.sleep(3)
        assert self.driver.current_url != self.url

    def test_calories_max(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("calories").clear()
        self.driver.find_element_by_name("calories").send_keys(create_input(9, is_number=True))
        self.driver.find_element_by_id("btn-ok-exercise").click()
        time.sleep(3)
        assert self.driver.current_url != self.url
        
    def test_calories_min(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("calories").clear()
        self.driver.find_element_by_name("calories").send_keys(create_input(0, is_number=True))
        self.driver.find_element_by_id("btn-ok-exercise").click()
        time.sleep(3)
        assert self.driver.current_url == self.url

    # cleanup method called after every test performed
    def tearDown(self):
        self.driver.close()
    
if __name__ == "__main__":
    unittest.main()
