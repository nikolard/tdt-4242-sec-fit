
import unittest
import string
import random
import requests
import itertools


def create_username():
    return ''.join(random.choice(string.ascii_letters) for i in range(10))

def create_input(length):
    return ''.join(random.choice(string.ascii_letters) for i in range(length))

password = create_input(8)

data =[
    [password, ""],
    ["Norge", ""],
    ["Klæbuveien", ""],
    ["Oslo", ""],
    ["12345678", ""],
]

domain =[
    [create_username(), create_input(151), ""],
    ["ian@ian.no", create_input(8), ""],
]

test_data = list(itertools.product(*data))

domain_data = list(itertools.product(*domain))

class RegisterTwoWayUnitTesting(unittest.TestCase):
    def setUp(self):
        self.counter = 0

    def test_two_way_domain(self):
        for d_data in domain_data:
            for t_data in test_data:
                response = requests.post("http://127.0.0.1:8000/api/users/", {'username': d_data[0].join(random.choice(string.ascii_letters) for i in range(5)), 'email': d_data[1], 'password': t_data[0], 'password1': t_data[0], 'country': t_data[1], 'street_address': t_data[2], 'city': t_data[3], 'phone_number': t_data[4]})
                if response.status_code == 201:
                    self.counter += 1
                    print('Combination number', self.counter, ':', 'username:', d_data[0], 'email:', d_data[1], 'password:',  t_data[0], 'country:', t_data[1], 'street address:', t_data[2], 'city:', t_data[3], 'phone number:', t_data[4])
                    
        assert(self.counter > 0)

            
    # cleanup method called after every test performed
    def tearDown(self):
        print("Test completed")
    
if __name__ == "__main__":
    unittest.main()
