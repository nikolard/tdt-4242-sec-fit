# Python program to demonstrate
# selenium
import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import string
import random


working_inputs = [{
    "input": "username",
    "value": "username",
},
{
    "input": "email",
    "value": "username@username.com",
},
{
    "input": "password",
    "value": "pass",
},
{
    "input": "password1",
    "value": "pass",
},
{
    "input": "phone_number",
    "value": "12345678",
},
{
    "input": "country",
    "value": "Norway",
},
{
    "input": "city",
    "value": "Trondheim",
},
{
    "input": "street_address",
    "value": "Street",
}]


def create_username():
    return ''.join(random.choice(string.ascii_letters) for i in range(10))

def create_input(length, is_number=False):
    return ''.join(random.choice(string.digits if is_number else string.ascii_letters) for i in range(length))

def insert_valid_inputs(driver):
    for input in working_inputs:
        if input["input"] == "username":
            input = create_username()
            driver.find_element_by_name("username").clear()
            driver.find_element_by_name("username").send_keys(input)
            continue
        
        driver.find_element_by_name(input["input"]).clear()
        driver.find_element_by_name(input["input"]).send_keys(input["value"])



class RegisterBVAUnitTesting(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.url = "http://localhost:5500/frontend/www/register.html"
        self.driver.get(self.url)
        self.driver.maximize_window()

    def test_create_user(self):
        insert_valid_inputs(self.driver)
        assert self.driver.current_url == self.url
    
    def test_no_username(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("username").clear()
        self.driver.find_element_by_id("btn-create-account").click()
        assert self.driver.current_url == self.url

    def test_username_over_150(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("username").clear()
        self.driver.find_element_by_name("username").send_keys(create_input(151))
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url == self.url

    def test_username_under_150(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("username").clear()
        self.driver.find_element_by_name("username").send_keys(create_input(149))
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url != self.url
    
    def test_email(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("email").clear()
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url == self.url

    def test_password_under_1(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("password").clear()
        self.driver.find_element_by_name("password").send_keys("")
        self.driver.find_element_by_name("password1").clear()
        self.driver.find_element_by_name("password1").send_keys("")

        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url == self.url

    def test_password_over_144(self):
        insert_valid_inputs(self.driver)
        input = create_input(145)
        self.driver.find_element_by_name("password").clear()
        self.driver.find_element_by_name("password").send_keys(input)
        self.driver.find_element_by_name("password1").clear()
        self.driver.find_element_by_name("password1").send_keys(input)

        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url != self.url

    def test_phone_over_50(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("phone_number").clear()
        self.driver.find_element_by_name("phone_number").send_keys(create_input(51))
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url == self.url

    def test_phone_under_8(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("phone_number").clear()
        self.driver.find_element_by_name("phone_number").send_keys(create_input(7, True))
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url != self.url

    def test_country_over_9(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("country").clear()
        self.driver.find_element_by_name("country").send_keys(create_input(9), True)
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url == self.url

    def test_country_under_50(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("country").clear()
        self.driver.find_element_by_name("country").send_keys(create_input(49))
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url != self.url

    def test_city_over_50(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("city").clear()
        self.driver.find_element_by_name("city").send_keys(create_input(51))
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url == self.url

    def test_city_under_50(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("city").clear()
        self.driver.find_element_by_name("city").send_keys(create_input(49))
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url != self.url
    def test_street_over_50(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("street_address").clear()
        self.driver.find_element_by_name("street_address").send_keys(create_input(51))
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url == self.url

    def test_street_under_50(self):
        insert_valid_inputs(self.driver)
        self.driver.find_element_by_name("street_address").clear()
        self.driver.find_element_by_name("street_address").send_keys(create_input(49))
        self.driver.find_element_by_id("btn-create-account").click()
        time.sleep(4)
        assert self.driver.current_url != self.url


    # cleanup method called after every test performed
    def tearDown(self):
        self.driver.close()
    
if __name__ == "__main__":
    unittest.main()
