# Python program to demonstrate
# selenium
import os
import time
import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import string
import random
from datetime import datetime
from selenium.webdriver.common.keys import Keys

today_date = datetime.today().strftime('%Y-%m-%d')

def login(driver, username, password):
    driver.find_element_by_name("username").clear()
    driver.find_element_by_name("username").send_keys(username)
    driver.find_element_by_name("password").clear()
    driver.find_element_by_name("password").send_keys(password)
    driver.find_element_by_id("btn-login").click()
    time.sleep(1)

def create_input(length):
    return ''.join(random.choice(string.ascii_letters) for i in range(length))

input = create_input(5);

class FR5UnitTesting(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.workout_name = input;

   

    def test_1_create_workout_user1(self):
        self.driver.get("http://localhost:5500/frontend/www/login.html")
        self.driver.maximize_window()
        login(self.driver, "admin", "admin")
        self.driver.get("http://localhost:5500/frontend/www/workout.html")
        time.sleep(2)

        
        date = '09052022'
        notes = "notes";
        comment = "This is a comment";


        self.driver.find_element_by_name("name").send_keys(self.workout_name)


        date_input = self.driver.find_element_by_name("date")
        date_input.send_keys(date)
        date_input.send_keys(Keys.TAB)
        date_input.send_keys("10:00")



        self.driver.find_element_by_name("notes").send_keys(notes)


        self.driver.find_element_by_name("files").send_keys(os.getcwd()+"/image.png")


        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(2)


        select = Select(self.driver.find_element_by_name('type'))
        time.sleep(2)
        select.select_by_visible_text('Push-up')            
        self.driver.find_element_by_name("sets").send_keys("3")

        self.driver.find_element_by_name("number").send_keys("10")
        time.sleep(2)

        self.driver.find_element_by_id("btn-add-exercise").click()
        time.sleep(2)

        self.driver.find_element_by_id("btn-ok-workout").click()
        time.sleep(2)

        list = self.driver.find_elements_by_class_name("workout")
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(2)

        for el in list:
            if self.workout_name in el.text:
                el.click()
                break
        time.sleep(2)

        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(2)

        self.driver.find_element_by_id("comment-area").send_keys(comment)

        self.driver.find_element_by_id("post-comment").click()
        time.sleep(2)

        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(2)
        assert(comment in self.driver.find_element_by_id("comment-list").text)
    
   
    def test_2_comment_and_file_workout_user2(self):
            self.driver.get("http://localhost:5500/frontend/www/login.html")
            self.driver.maximize_window()
            login(self.driver, "ian", "ian")

            time.sleep(2)
            self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")

            list = self.driver.find_elements_by_class_name("workout")
            for el in list:
                if self.workout_name in el.text:
                    el.click()
                    break
            time.sleep(2)


            name = self.driver.find_element_by_name("name").text
            date = self.driver.find_element_by_name("date").text
            notes = self.driver.find_element_by_name("notes").text

            self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(2)
            comment = self.driver.find_element_by_id("comment-list").text

            assert(name is not None and date is not None and notes is not None and comment is not None)
    
        
        
    # cleanup method called after every test performed
    def tearDown(self):
        self.driver.close()
    
if __name__ == "__main__":
    unittest.main()
