# Python program to demonstrate
# selenium
import time
import unittest
from selenium import webdriver

def login(driver):
    driver.find_element_by_name("username").clear()
    driver.find_element_by_name("username").send_keys("admin")
    driver.find_element_by_name("password").clear()
    driver.find_element_by_name("password").send_keys("admin")
    driver.find_element_by_id("btn-login").click()
    time.sleep(1)



class ProfileIntegrationUnitTesting(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.url = "http://localhost:5500/frontend/www/login.html"
        self.driver.get(self.url)
        self.driver.maximize_window()
        login(self.driver)
        self.url = "http://localhost:5500/frontend/www/profile.html"
        self.driver.get(self.url)
        time.sleep(2)

    def test_integration_profile(self):
        name = self.driver.find_element_by_name("username").get_attribute('value')
        email = self.driver.find_element_by_name("email").get_attribute('value')
        phone = self.driver.find_element_by_name("phone_number").get_attribute('value')
        address = self.driver.find_element_by_name("address").get_attribute('value')

        result = name == "admin" and email == "N/A" and phone == "N/A" and address == "N/A"
        assert(result)

    # cleanup method called after every test performed
    def tearDown(self):
        self.driver.close()
    
if __name__ == "__main__":
    unittest.main()
