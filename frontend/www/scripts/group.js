async function generateGroupForm() {
    let form = document.querySelector("#form-group");

    let formData = new FormData(form);
    let submitForm = new FormData();

    let input = formData.get("group");
    let groupName = input.split(":")[0];
    submitForm.append("name", groupName);

    let athletes = input.split(":")[1].replaceAll(" ", "").split(",");
    let currentUser = await getCurrentUser();

    for (let athlete of athletes) {
        let response = await sendRequest(
            "GET",
            `${HOST}/api/users/${athlete}/`
        );
        let result = await response.json();
        submitForm.append("athletes", result.url);
    }

    console.log(currentUser.url);
    submitForm.append("coach", currentUser.url);
    return submitForm;
}

async function createGroup() {
    let submitForm = await generateGroupForm();

    let response = await sendRequest(
        "POST",
        `${HOST}/api/groups/`,
        submitForm,
        ""
    );

    if (response.ok) {
        document.getElementById("group-input").value = "";
        window.location.reload();
        return;
    } else {
        let data = await response.json();
        let alert = createAlert("Could not create new workout!", data);
        document.body.prepend(alert);
    }
}
