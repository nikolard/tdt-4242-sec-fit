async function fetchChallenges() {
    let response = await sendRequest("GET", `${HOST}/api/challenges/`);
    let currentUser = await getCurrentUser();

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        let data = await response.json();

        let challenges = data.results;
        let container = document.getElementById("challenge-control");

        let templateChallenge = document.querySelector(
            "#template-challenge-joined"
        );

        challenges
            .filter((challenge) => challenge.athlete == currentUser.url)
            .forEach((challenge) => {
                let cloneChallenge = templateChallenge.content.cloneNode(true);
                let titleChallenge = cloneChallenge.querySelector("h5");
                let statusChallenge = cloneChallenge.querySelector(
                    "#card-body-completed"
                );

                // let h5 = titleChallenge.querySelector("h5");
                titleChallenge.innerHTML = challenge.title;
                statusChallenge.innerHTML = challenge.completed
                    ? "Completed"
                    : "Not Completed";

                container.appendChild(cloneChallenge);
            });
        return challenges;
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    await fetchChallenges();

    // let buttonSetCoach = document.querySelector("#button-set-coach");
    // let buttonEditCoach = document.querySelector("#button-edit-coach");
    // let buttonCancelCoach = document.querySelector("#button-cancel-coach");

    // buttonSetCoach.addEventListener(
    //     "click",
    //     async (event) => await setCoach(event)
    // );
    // buttonEditCoach.addEventListener("click", editCoach);
    // buttonCancelCoach.addEventListener("click", cancelCoach);
});
