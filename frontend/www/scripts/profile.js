async function displayUser() {
    let user = await getCurrentUser();

    if (user) {
        let form = document.querySelector("#form-user");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector;
            key !== "muscleGroup"
                ? (selector = `input[name="${key}"], textarea[name="${key}"]`)
                : (selector = `select[name=${key}]`);
            let input = form.querySelector(selector);
            let newVal = user[key];
            if (!newVal) newVal = "N/A";
            input.value = newVal;
        }
        document.querySelector("select").setAttribute("disabled", "");
    } else {
        let alert = createAlert("Could not find user", {
            detail: "No user is found",
        });
        document.body.prepend(alert);
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    await displayUser();

    // let buttonSetCoach = document.querySelector("#button-set-coach");
    // let buttonEditCoach = document.querySelector("#button-edit-coach");
    // let buttonCancelCoach = document.querySelector("#button-cancel-coach");

    // buttonSetCoach.addEventListener(
    //     "click",
    //     async (event) => await setCoach(event)
    // );
    // buttonEditCoach.addEventListener("click", editCoach);
    // buttonCancelCoach.addEventListener("click", cancelCoach);
});
