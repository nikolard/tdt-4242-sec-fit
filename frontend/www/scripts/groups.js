async function getGroups() {
    let groups = null;
    let response = await sendRequest("GET", `${HOST}/api/groups/`);
    if (!response.ok) {
        console.log("COULD NOT RETRIEVE GROUPS");
    } else {
        let data = await response.json();
        groups = data.results;
    }

    return groups;
}
