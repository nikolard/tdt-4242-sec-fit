async function generateChallengeForm(athlete) {
    let form = document.querySelector("#form-challenge");

    let formData = new FormData(form);
    let submitForm = new FormData();

    let inputTitle = formData.get("challenge-title");
    submitForm.append("title", inputTitle);

    let response = await sendRequest("GET", `${HOST}/api/users/${athlete}/`);
    let result = await response.json();
    submitForm.append("athlete", result.url);

    let currentUser = await getCurrentUser();
    submitForm.append("coach", currentUser.url);
    return submitForm;
}

async function createChallenge() {
    let inputAthletes = document.querySelector("#challenge-user-input").value;
    console.log(inputAthletes);
    let athletes = inputAthletes.replaceAll(" ", "").split(",");

    for (let athlete of athletes) {
        let submitForm = await generateChallengeForm(athlete);
        let response = await sendRequest(
            "POST",
            `${HOST}/api/challenges/`,
            submitForm,
            ""
        );

        if (response.ok) {
            console.log("GUCCI");
        } else {
            let data = await response.json();
            let alert = createAlert("Could not create new challenge!", data);
            document.body.prepend(alert);
        }
    }
    document.getElementById("challenge-title-input").value = "";
    document.getElementById("challenge-user-input").value = "";
    window.location.reload();
    return;
}
