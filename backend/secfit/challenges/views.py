from rest_framework import mixins, generics
from challenges.models import Challenge
from challenges.serializers import ChallengeSerializer, ChallengeGetSerializer

# Create your views here

class ChallengeList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    serializer_class = ChallengeSerializer


    def get(self, request, *args, **kwargs):
        self.serializer_class = ChallengeGetSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_queryset(self):
        # Tror man filtrerer her
        return Challenge.objects.all()


class ChallengeDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    lookup_field_options = ["pk"]
    # serializer_class = UserSerializer
    # queryset = get_user_model().objects.all()
    # permission_classes = [permissions.IsAuthenticated & (IsCurrentUser | IsReadOnly)]

    def get_object(self):
        for field in self.lookup_field_options:
            if field in self.kwargs:
                self.lookup_field = field
                break

        return super().get_object()

    def get(self, request, *args, **kwargs):
        #self.serializer_class = UserGetSerializer
        return self.retrieve(request, *args, **kwargs)
