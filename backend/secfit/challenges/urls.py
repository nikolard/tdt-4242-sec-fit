from django.urls import path
from challenges import views

urlpatterns = [
    path("api/challenges/", views.ChallengeList.as_view(), name="challenge-list"),
    path("api/challenges/<int:pk>/", views.ChallengeDetail.as_view(), name="challenge-detail"),
]
