from rest_framework import serializers
from challenges.models import Challenge

class ChallengeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Challenge
        fields = [
            "title",
            "athlete",
            "coach",
            "completed"
        ]
    
    def create(self, validated_data):
        return Challenge.objects.create(**validated_data)



class ChallengeGetSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Challenge
        fields = [
            "title",
            "athlete",
            "coach",
            "completed"
        ]