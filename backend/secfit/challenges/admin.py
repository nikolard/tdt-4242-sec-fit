from django.contrib import admin
from challenges.models import Challenge

# Register your models here.
admin.site.register(Challenge)
