from django.db import models
from django.contrib.auth import get_user_model


# Create your models here.
class Challenge(models.Model):
    """Django model for a group of athletes

    Each group has a coach

    Attributes:
        coach:       Who coaches the group
        title:       Name of group
        athletes     All athletes that are assigned to challenge
    """
    title = models.CharField(max_length=24, default="Challenge")
    coach = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="+")
    athlete = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,  blank=True, null=True)
    completed = models.BooleanField(default=False)
