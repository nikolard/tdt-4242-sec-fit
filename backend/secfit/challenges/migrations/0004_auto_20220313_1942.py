# Generated by Django 3.1 on 2022-03-13 19:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('challenges', '0003_auto_20220313_1647'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='challenge',
            name='athletes',
        ),
        migrations.AddField(
            model_name='challenge',
            name='athlete',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='challenge',
            name='completed',
            field=models.BooleanField(default=False),
        ),
        migrations.DeleteModel(
            name='ChallengeCompleted',
        ),
    ]
