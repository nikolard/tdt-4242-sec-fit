"""
Tests for the workouts application.
"""
from django.test import TestCase
from workouts.permissions import IsOwner, IsOwnerOfWorkout, IsCoachAndVisibleToCoach, IsCoachOfWorkoutAndVisibleToCoach, IsPublic, IsWorkoutPublic, IsReadOnly
from workouts.models import Workout
from users.models import User
from django.utils import timezone


class Obj():
    def __init__(self, owner, workout=None, visibility="PU"):
        self.owner = owner
        self.workout = workout
        self.visibility = visibility

class Request():
    def __init__(self, user, data, method="GET"):
        self.user = user
        self.method = method
        self.data = data

# Create your tests here.
class WorkoutTestCase(TestCase):
    def setUp(self):
        coach = User.objects.create(username="Coach", password="coach")
        user = User.objects.create(username="Niko", password="niko", coach=coach)
        otherUser = User.objects.create(username="Ian", password="ian")
        
        workout = Workout.objects.create(date=timezone.now(), owner_id=user.id, visibility="PU")
        workoutUrl = "http://noe:8000/api/workouts/" + str(workout.id) + "/"
        
        self.request = Request(user=user, data={"workout": workoutUrl})
        self.requestWithoutValidWorkout = Request(user=user, data={"test": None})
        self.otherRequest = Request(user=otherUser, data={"workout": workoutUrl})
        self.coachRequest = Request(user=coach, data={"workout": workoutUrl})
        self.obj = Obj(owner=user, workout=workout)
        
    def test_is_owner(self):
        """User is correctly created"""
        has_permission = IsOwner().has_object_permission(request=self.request,view={}, obj=self.obj)
        self.assertEqual(has_permission, True)

    def test_is_owner_of_workout(self):
        self.request.method = "POST"
        has_permission = IsOwnerOfWorkout().has_permission(self.request, {})
        self.assertEqual(has_permission, True)
        
        self.otherRequest.method = "POST"
        has_permission = IsOwnerOfWorkout().has_permission(self.otherRequest, {})
        self.assertEqual(has_permission, False)
        
        self.request.method = "GET"
        has_permission = IsOwnerOfWorkout().has_permission(self.request, {})
        self.assertEqual(has_permission, True)

        has_permission = IsOwnerOfWorkout().has_object_permission(self.request, {}, self.obj)
        self.assertEqual(has_permission, True)

        self.requestWithoutValidWorkout.method = "POST"
        has_permission = IsOwnerOfWorkout().has_permission(self.requestWithoutValidWorkout, {})
        self.assertEqual(has_permission, False)
    
    def test_is_coach_and_visible_to_coach(self):
        has_permission = IsCoachAndVisibleToCoach().has_object_permission(self.request, {}, self.obj)
        self.assertEqual(has_permission, False)
        
        has_permission = IsCoachAndVisibleToCoach().has_object_permission(self.coachRequest, {}, self.obj)
        self.assertEqual(has_permission, True)

    def test_is_coach_of_workout_and_visible_to_coach(self):
        has_permission = IsCoachOfWorkoutAndVisibleToCoach().has_object_permission(self.coachRequest, {}, self.obj)
        self.assertEqual(has_permission, True)
        
        has_permission = IsCoachOfWorkoutAndVisibleToCoach().has_object_permission(self.request, {}, self.obj)
        self.assertEqual(has_permission, False)

    def test_is_public(self):
        is_public = IsPublic().has_object_permission(self.request, {}, self.obj)
        self.assertEqual(is_public, True)
        
        self.obj.visibility = "PR"
        is_public = IsPublic().has_object_permission(self.request, {}, self.obj)
        self.assertEqual(is_public, False)
    
    def test_is_workout_public(self):        
        is_public = IsWorkoutPublic().has_object_permission(self.request, {}, self.obj)
        self.assertEqual(is_public, True)
       
        self.obj.workout.visibility = "PR"
        is_public = IsWorkoutPublic().has_object_permission(self.request, {}, self.obj)
        self.assertEqual(is_public, False)

    def test_is_read_only(self):
        is_read_only = IsReadOnly().has_object_permission(self.request, {}, self.obj)
        self.assertEqual(is_read_only, True)
        
        self.request.method = "POST"
        is_read_only = IsReadOnly().has_object_permission(self.request, {}, self.obj)
        self.assertEqual(is_read_only, False)
