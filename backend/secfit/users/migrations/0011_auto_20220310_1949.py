# Generated by Django 3.1 on 2022-03-10 19:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0010_group_groupmembership'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='group',
            name='coach',
        ),
        migrations.AddField(
            model_name='group',
            name='name',
            field=models.CharField(default='Group', max_length=24),
        ),
        migrations.AlterField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(to='users.Group'),
        ),
        migrations.DeleteModel(
            name='GroupMembership',
        ),
    ]
