from django.test import TestCase

from users.serializers import UserSerializer, UserPutSerializer, AthleteFileSerializer
from users.models import User

# Create your tests here.


class UserTestCase(TestCase):
    def setUp(self):
        # Animal.objects.create(name="lion", sound="roar")
        # Animal.objects.create(name="cat", sound="meow")
        # get_user_model().objects.create()
        self.user_data = {
            "username": "NikoTest",
            "password": "",
            "email": "nikolaidokken",
            "phone_number": "12345678",
            "country": "Norway",
            "city": "Baerum",
            "street_address": "Veien 2"
            }
        self.testUser = User.objects.create(username="Niko", password="niko")
        
    def test_user_is_created(self):
        """User is correctly created"""
        newUser = UserSerializer().create(validated_data=self.user_data)
        foundUser = User.objects.get(username="NikoTest")
        self.assertEqual(newUser, foundUser)

    def test_user_valid_password(self):
        serializer = UserSerializer(data=self.user_data)
        self.assertEqual(serializer.validate_password(True), True)

    def test_user_put_athletes(self):
        newAthlete = User.objects.create(username="athlete", password="athlete")
        UserPutSerializer().update(self.testUser, {"athletes": [newAthlete]})
        
        self.assertEqual(self.testUser.athletes.get(), newAthlete)

    def test_create_athlete_file(self):
        AthleteFileSerializer().create({"athlete": self.testUser, "owner_id": self.testUser.id})
