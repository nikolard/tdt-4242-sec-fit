from curses.ascii import US
from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.
class Group(models.Model):
    """Django model for a group of athletes

    Each group has a coach

    Attributes:
        coach:       Who coaches the group
        name:        Name of group
    """
    coach = models.ForeignKey(
        "users.User", on_delete=models.CASCADE, related_name="coached_groups")
    name = models.CharField(max_length=24, default="Group")
    athletes = models.ManyToManyField(get_user_model())