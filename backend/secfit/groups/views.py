from rest_framework import mixins, generics
from groups.models import Group
from groups.serializers import GroupSerializer, GroupGetSerializer
# Create your views here

class GroupList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    serializer_class = GroupSerializer


    def get(self, request, *args, **kwargs):
        self.serializer_class = GroupGetSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_queryset(self):
        # Tror man filtrerer her
        qs = Group.objects.all()

        if self.request.user:
            # Return the currently logged in user
            # status = self.request.query_params.get("user", None)
            qs = Group.objects.filter(coach=self.request.user.pk)

        return qs

class GroupDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    lookup_field_options = ["pk"]
    # serializer_class = UserSerializer
    # queryset = get_user_model().objects.all()
    # permission_classes = [permissions.IsAuthenticated & (IsCurrentUser | IsReadOnly)]

    def get_object(self):
        for field in self.lookup_field_options:
            if field in self.kwargs:
                self.lookup_field = field
                break

        return super().get_object()

    def get(self, request, *args, **kwargs):
        #self.serializer_class = UserGetSerializer
        return self.retrieve(request, *args, **kwargs)
