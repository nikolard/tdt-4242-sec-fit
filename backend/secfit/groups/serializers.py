from rest_framework import serializers
from groups.models import Group
from django.contrib.auth import get_user_model

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = [
            "name",
            "athletes",
            "coach",
        ]
    
    def create(self, validated_data):

        files_data = []
        if "athletes" in validated_data:
            files_data = validated_data.pop("athletes")

        group_obj = Group.objects.create(**validated_data)
        group_obj.athletes.set(files_data)

        return group_obj


class GroupGetSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = [
            "name",
            "athletes",
            "coach",
        ]